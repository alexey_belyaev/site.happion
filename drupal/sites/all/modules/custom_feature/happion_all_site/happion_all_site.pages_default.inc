<?php
/**
 * @file
 * happion_all_site.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function happion_all_site_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'home_page';
  $page->task = 'page';
  $page->admin_title = 'Home page';
  $page->admin_description = 'custom homepage ';
  $page->path = 'home';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_home_page_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'home_page';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'threecol_33_34_33_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '7cb4f75a-af50-4884-adb3-7d567d7e1b44';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-a5447b61-880d-47a7-9f27-26b20c868663';
    $pane->panel = 'left';
    $pane->type = 'views';
    $pane->subtype = 'front_page_block_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '1',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 1,
      'override_title_text' => '<none>',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a5447b61-880d-47a7-9f27-26b20c868663';
    $display->content['new-a5447b61-880d-47a7-9f27-26b20c868663'] = $pane;
    $display->panels['left'][0] = 'new-a5447b61-880d-47a7-9f27-26b20c868663';
    $pane = new stdClass();
    $pane->pid = 'new-a6938924-3ea2-4f33-9647-c12a4c48e44b';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'front_page_block_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '1',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_1',
      'override_title' => 1,
      'override_title_text' => '<none>',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a6938924-3ea2-4f33-9647-c12a4c48e44b';
    $display->content['new-a6938924-3ea2-4f33-9647-c12a4c48e44b'] = $pane;
    $display->panels['middle'][0] = 'new-a6938924-3ea2-4f33-9647-c12a4c48e44b';
    $pane = new stdClass();
    $pane->pid = 'new-65a67472-fac0-489d-aad3-603a37ba67c3';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'blog';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '3',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_1',
      'override_title' => 1,
      'override_title_text' => '<none>',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '65a67472-fac0-489d-aad3-603a37ba67c3';
    $display->content['new-65a67472-fac0-489d-aad3-603a37ba67c3'] = $pane;
    $display->panels['right'][0] = 'new-65a67472-fac0-489d-aad3-603a37ba67c3';
    $pane = new stdClass();
    $pane->pid = 'new-2fe16d00-784d-4674-badf-d95c68655c80';
    $pane->panel = 'top';
    $pane->type = 'views';
    $pane->subtype = 'frontpage_slideshow';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '5',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2fe16d00-784d-4674-badf-d95c68655c80';
    $display->content['new-2fe16d00-784d-4674-badf-d95c68655c80'] = $pane;
    $display->panels['top'][0] = 'new-2fe16d00-784d-4674-badf-d95c68655c80';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-2fe16d00-784d-4674-badf-d95c68655c80';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['home_page'] = $page;

  return $pages;

}
