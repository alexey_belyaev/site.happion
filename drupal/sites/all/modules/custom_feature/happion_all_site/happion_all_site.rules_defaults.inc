<?php
/**
 * @file
 * happion_all_site.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function happion_all_site_default_rules_configuration() {
  $items = array();
  $items['rules_article_require_approval'] = entity_import('rules_config', '{ "rules_article_require_approval" : {
      "LABEL" : "article require approval",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_presave" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "article" : "article", "page" : "page" } }
          }
        },
        { "NOT user_has_role" : { "account" : [ "node:author" ], "roles" : { "value" : { "3" : "3" } } } },
        { "NOT user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "3" : "3" } }
          }
        }
      ],
      "DO" : [
        { "drupal_message" : { "message" : "Your text was sent to admin to approve it" } },
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "3" : "3" } },
            "subject" : "Approve please content",
            "message" : "Node need approval [node:url]"
          }
        },
        { "node_unpublish" : { "node" : [ "node" ] } }
      ]
    }
  }');
  $items['rules_article_require_approval_by_admin_'] = entity_import('rules_config', '{ "rules_article_require_approval_by_admin_" : {
      "LABEL" : "article require approval (by admin)",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "1",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_presave" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "article" : "article", "page" : "page" } }
          }
        },
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "3" : "3" } }
          }
        },
        { "data_is" : { "data" : [ "node:status" ], "value" : "0" } }
      ],
      "DO" : [
        { "drupal_message" : { "message" : "Your recently approved node [node:title]" } },
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "3" : "3" } },
            "subject" : "Node has been approved",
            "message" : "Node [node:url] has been approved"
          }
        },
        { "node_publish" : { "node" : [ "node" ] } }
      ]
    }
  }');
  return $items;
}
